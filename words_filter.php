<?php
/**
 * Plugin Name:       words filter
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       this plugin helps you to filter and replace words
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Alireza Farjami
 * Author URI:        https://author.example.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Update URI:        https://example.com/my-plugin/
 * Text Domain:       words-filter
 * Domain Path:       /languages
 */


define ('WF_8971_DIR', plugin_dir_path(__FILE__));
define ('WF_8971_URL', plugin_dir_url(__FILE__));
define ('WF_8971_INC', WF_8971_DIR.'/inc/');

if(is_admin()){
include WF_8971_INC.'admin/menus.php';
}
else {
    include WF_8971_INC.'user/menus.php';}
include WF_8971_INC.'common/menus.php';

function wf_8971_words ($content) {
    $word = "WordPress";
    $replace = "WP";
    $content = preg_replace("/{$word}/", $replace, $content);
    return $content;
}

add_filter ('the_content','wf_8971_words');